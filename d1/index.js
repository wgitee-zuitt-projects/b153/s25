// Import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.

let http = require("http")


//the createServer() methos lets you create an HTTP server that listens to requests on a specified port, and also send responses back to the client. 

//user sends a request to view a page on port 4000 -> createServer() reacts to this request by executing our code, which sends the response.

const server = http.createServer( function (request, response){

	// Use writeHead() method to:
		//Set a status code for the response - 200 means OK
		//Set the content-type of the response as a plain text message


	response.writeHead(200, {'content-Type': 'text/plain'})

	//send a response with the text content
	response.end("Hello Class!")
})


const port = 4000

/*
	Port Numbers:

	0 - 1023: Well-know ports are kept reserved/never used because they are typically used for connections important for the system.

	1024 - 65535 can be used

	IF an application is running on port 4000, no other application can run on the same port, as the port is busy
*/

server.listen(port) // if a user tries to access port 4000, send the reponse that we added in createServer



//show a confirmation message that the server is running on the assigned port
console.log(`Server running at port ${port}`)
