// Import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.

let http = require("http")


const server = http.createServer( function (request, response){

	/*
		Terminology note:

		URL: localhost:4000/greeting
		Origin: localhost:4000
		Endpoint: /greeting
	*/


	if(request.url === '/greeting'){

	response.writeHead(200, {'content-Type': 'text/plain'})

	response.end("Hello Will!")


	}else if(request.url === '/'){ //A single slash means no endpoint was added to the URL/origin

	response.writeHead(200, {'content-Type': 'text/plain'})

	response.end("This is the homepage!")

	}else{
	response.writeHead(404, {'content-Type': 'text/plain'})

	response.end("Page not found.")

	//Adjusting our code to handle ANY endpoint besides our homepage and /greeting to send the browser a 404 code and show a message that says "Page not found"

	}
})

const port = 4000

server.listen(port) 

console.log(`Server running at port ${port}`)
